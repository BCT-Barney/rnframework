import * as React from 'react';
import { ViewProps } from 'react-native';
export declare function SafeAreaView({ style, ...rest }: ViewProps & {
    children: React.ReactNode;
}): JSX.Element;
