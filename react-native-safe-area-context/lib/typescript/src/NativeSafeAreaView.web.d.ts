/// <reference types="react" />
import { NativeSafeAreaViewProps } from './SafeArea.types';
export default function NativeSafeAreaView({ children, style, onInsetsChange, }: NativeSafeAreaViewProps): JSX.Element;
