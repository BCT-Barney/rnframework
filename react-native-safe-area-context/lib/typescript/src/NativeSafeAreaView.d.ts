import { NativeSafeAreaViewProps } from './SafeArea.types';
declare const _default: import("react-native").HostComponent<NativeSafeAreaViewProps>;
export default _default;
