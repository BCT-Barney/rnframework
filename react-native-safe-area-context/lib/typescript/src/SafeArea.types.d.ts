/// <reference types="react" />
import { NativeSyntheticEvent, ViewStyle } from 'react-native';
export interface EdgeInsets {
    top: number;
    right: number;
    bottom: number;
    left: number;
}
export interface Rect {
    x: number;
    y: number;
    width: number;
    height: number;
}
export interface Metrics {
    insets: EdgeInsets;
    frame: Rect;
}
export declare type InsetChangedEvent = NativeSyntheticEvent<Metrics>;
export declare type InsetChangeNativeCallback = (event: InsetChangedEvent) => void;
export interface NativeSafeAreaViewProps {
    children?: React.ReactNode;
    style?: ViewStyle;
    onInsetsChange: InsetChangeNativeCallback;
}
