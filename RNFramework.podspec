Pod::Spec.new do |s|
  s.name = 'RNFramework'
  s.version = '0.0.1'
  s.summary = 'RN frameworks'
  s.description = <<-DESC
  For SDK usage
                   DESC

  s.homepage = 'https://gitlab.com/BCT-Barney/rnframework'
  s.license = 'MIT'
  s.authors = { 'BCT-Barney' => 'barney.chen@bahwancybertek.com' }

  s.ios.deployment_target = '12.0'

  s.swift_version = '5.0'
  s.requires_arc = true
  s.source = { git: 'https://gitlab.com/BCT-Barney/rnframework.git', tag: s.version.to_s }
  s.source_files = '**'

  # s.dependency 
  # SwiftStdlib Extensions
  # s.subspec 'SwiftStdlib' do |sp|
  #   sp.source_files  = 'Sources/SwifterSwift/Shared/*.swift', 'Sources/SwifterSwift/SwiftStdlib/*.swift'
  # end

  # spec.source = { :git => 'https://github.com/google/double-conversion.git',
  #                 :tag => "v#{spec.version}" }

  # # Foundation Extensions
  # s.subspec 'Foundation' do |sp|
  #   sp.source_files  = 'Sources/SwifterSwift/Shared/*.swift', 'Sources/SwifterSwift/Foundation/*.swift'
  # end

  # # UIKit Extensions
  # s.subspec 'UIKit' do |sp|
  #   sp.source_files  = 'Sources/SwifterSwift/Shared/*.swift', 'Sources/SwifterSwift/UIKit/*.swift'
  # end

  # # AppKit Extensions
  # s.subspec 'AppKit' do |sp|
  #   sp.source_files  = 'Sources/SwifterSwift/Shared/*.swift', 'Sources/SwifterSwift/AppKit/*.swift'
  # end

  # # CoreGraphics Extensions
  # s.subspec 'CoreGraphics' do |sp|
  #   sp.source_files  = 'Sources/SwifterSwift/CoreGraphics/*.swift'
  # end

  # # CoreLocation Extensions
  # s.subspec 'CoreLocation' do |sp|
  #   sp.source_files  = 'Sources/SwifterSwift/CoreLocation/*.swift'
  # end

  # # CoreAnimation Extensions
  # s.subspec 'CoreAnimation' do |sp|
  #   sp.source_files  = 'Sources/SwifterSwift/Shared/*.swift', 'Sources/SwifterSwift/CoreAnimation/*.swift'
  # end

  # # MapKit Extensions
  # s.subspec 'MapKit' do |sp|
  #   sp.source_files = 'Sources/SwifterSwift/Shared/*.swift', 'Sources/SwifterSwift/MapKit/*.swift'
  # end

  # # SpriteKit Extensions
  # s.subspec 'SpriteKit' do |sp|
  #   sp.source_files = 'Sources/SwifterSwift/SpriteKit/*.swift'
  # end

  # s.subspec 'SceneKit' do |sp|
  #   sp.source_files  =  'Sources/SwifterSwift/Shared/*.swift', 'Sources/SwifterSwift/SceneKit/*.swift'
  # end

  # # StoreKit Extensions
  # s.subspec 'StoreKit' do |sp|
  #   sp.source_files = 'Sources/SwifterSwift/StoreKit/*.swift'
  # end

  # # Dispatch Extensions
  # s.subspec 'Dispatch' do |sp|
  #   sp.source_files = 'Sources/SwifterSwift/Dispatch/*.swift'
  # end

end
